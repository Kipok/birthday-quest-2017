<?php
    session_start();
    if (!isset($_SESSION['logged_name'])) {
        header("location:index.php");
        exit;
    }
    require('load_question.php');
    $riddle_num = $_SESSION['riddle_num'];
?>
<html>
<head>
    <meta charset="UTF-8">
    <link href="style/main.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="js/main.js"></script>
    <title>В поисках подарка..</title>
</head>
<body>
    <div class="background-img" id="img0" style='display: <?php if ($riddle_num > 0) echo 'none'; else echo 'block'; ?>'></div>
	<div class="background-img" id="img1" style='display: <?php if ($riddle_num > 1 || $riddle_num < 1) echo 'none'; else echo 'block'; ?>'></div>
	<div class="background-img" id="img2" style='display: <?php if ($riddle_num > 2 || $riddle_num < 2) echo 'none'; else echo 'block'; ?>'></div>
	<div class="background-img" id="img3" style='display: <?php if ($riddle_num > 3 || $riddle_num < 3) echo 'none'; else echo 'block'; ?>'></div>
	<div class="background-img" id="img4" style='display: <?php if ($riddle_num > 4 || $riddle_num < 4) echo 'none'; else echo 'block'; ?>'></div>
	<div id="quest-task">
        <div id="question-start">
           Четыре кусочка пазла — четыре загадки. Справишься? ;) 
        </div>
        <div id="question-end">
            <?php
                echo $question[$riddle_num];
            ?>
        </div>
        <div id="submit-div">
	    	<button id='submit-button'>Проверить</button>
            <input id="answer-input" name="answer" type="text" size="40" autocomplete="off" spellcheck="false">
        </div>
	</div>
<?php
    if ($riddle_num == 0) {
        echo "
        <div id='hint-div'>
            Хорошее начало! Все оказалось не так уж и сложно. Ответ: ближе, чем ты думаешь
        </div>";
    }
?>
</body>
</html>
