function check_input() {
    answer = $('#answer-input').val();
    $.ajax({
        type: 'post',
        url: 'check_answer.php',
        data: {'answer': answer},
        response: 'text',
        success:function (data) {
            if (data != 'false') {
                if (data[0] == '#') {
                    $(data.substring(0, 5)).fadeIn(2000);
                    $('#answer-input').val('');
                    $('#question-end').fadeOut(1000, function(){
                        $('#question-end').html(data.substring(5, data.length));
                        $('#question-end').fadeIn(1000);
                    });
                    if (data.substring(0, 5) == "#img1") {
                        $('#hint-div').hide();
                    }
                } else {
                    $('body').append(data);
            	    document.getElementById('bgvid').addEventListener('ended', function() {
            	    	$('#present-div').fadeIn(700);
            	    });
                    $('#quest-task').fadeTo(1000, 0.0, function() {
            		    $('#bgvid').fadeIn(4000, function() {
        		    	    $('#bgvid')[0].play();
        		        });
                    });
                }
            } else {
                $('#answer-input').val('');
            }
        }
    });
}

window.onload = function() {
    function preload(arrayOfImages) {
        $(arrayOfImages).each(function(){
            $('<img/>')[0].src = this;
        });
    }
    
    preload([
        'media/bg0.png',
        'media/bg1.png',
        'media/bg2.png',
        'media/bg3.png',
        'media/bg4.png',
        'media/poster.png',
    ]);

    $('#answer-input').keydown(function (e){
        if(e.keyCode == 13){
            $("#submit-button").click();
        }
    });

	document.getElementById('submit-button').addEventListener('click', function() {
        check_input();
	});
};

