<?php
    session_start();
    if (isset($_SESSION['logged_name'])) {
        header("location:main.php");
    }
?>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <link href="style/main.css" rel="stylesheet">
    <title>В поисках подарка..</title>
</head>
<body>
    <div id='login-div' style='font-family: 'calibri'; font-size: 20px;'>
        <form name="login" method="post" action="login.php">
            Имя:<br>
            <input type="text" name="username" style='margin-bottom: 10px; height: 28px; width: 245px;'><br>
            Пароль:<br>
            <input type="password" name="password" placeholder="В начале день" style='margin-bottom: 10px; height: 28px; width: 245px;'><br>
            <input type="submit" value="Войти" style="float:right; height: 30px; width: 74px; font-family: 'calibri'; font-size: 17px;"><br>
            <?php
                if (isset($_SESSION['error'])) {
                    echo $_SESSION['error'];
                    unset($_SESSION['error']);
                }
            ?>
        </form> 
    </div>
</body>
</html>
