<?php
session_start();
require('load_question.php'); 

$myfile = fopen("logs/tries-{$_SESSION['riddle_num']}.txt", "a");
fwrite($myfile, $_POST['answer'] . "\n");
fclose($myfile);

if (mb_strtolower($_POST['answer']) == $answer[$_SESSION['riddle_num']]) {
    if ($_SESSION['riddle_num'] < 4) {
        // TODO: not generalizable to many users
        $db->query("UPDATE anna_progress SET riddle_num = riddle_num+1 WHERE name = 'anna'");
        $_SESSION['riddle_num'] += 1;
        echo "#img" . $_SESSION['riddle_num'] . ' ' . $question[$_SESSION['riddle_num']];
    } else {
        echo '
    <video poster="media/poster.png" id="bgvid" playsinline muted>
        <source src="media/video.mp4" type="video/mp4">
    </video>
    <div id="present-div">
        С днем рождения!<br>
        Подарок <a href="media/book.mobi">тут</a> :)
    </div>';
    }
} else {
    echo "false";
}
?>
