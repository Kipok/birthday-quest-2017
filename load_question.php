<?php
    define('DB_SERVER', 'localhost');
    define('DB_USERNAME', 'quest.cs.msu.ru');
    define('DB_PASSWORD', 'VsNV4yn4QUbPcxN8');
    define('DB_DATABASE', 'quest.cs.msu.ru');
    $db = new mysqli(DB_SERVER,DB_USERNAME,DB_PASSWORD,DB_DATABASE);
    if ($db->connect_error) {
        die("Connection failed: " . $db->connect_error);
    }

    $sqlp = $db->prepare("SELECT riddle_num FROM anna_progress WHERE name=?");
    $sqlp->bind_param('s', $_SESSION['logged_name']);
    $sqlp->execute();
    $sqlp->store_result();
    $sqlp->bind_result($_SESSION['riddle_num']);
    $sqlp->fetch();
    $sqlp->close();

    $question = array(
        0 => 'Внимание.<br>
        Ответ на первую загадку находится прямо на этой странице. Нужно только суметь выделить его среди бесполезного шума..',
        1 => 'Знания.<br><img src="media/question2.png"></img>',
        2 => 'Вера.<br>мк 12 17. Ответ?',
        3 => 'Память.<br>Я бы сегодня выбрал борщ, тыквенный пирог и овсяную кашу с яблоками и корицей!',
        4 => 'Ура! Ты все разгадала :) Осталось ответить на последний вопрос: кто нарисован на картинке?',
    );
    $answer = array(
        0 => 'ближе, чем ты думаешь',
        1 => 'западная стена',
        2 => 'отдавайте кесарево кесарю, а божие богу',
        3 => 'сим-сим откройся',
        4 => 'жираф',
    );
?>
